﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

using System;
using System.Collections.Generic;
using Xunit;
using FluentAssertions;
using Microsoft.Data.Sqlite;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using AutoFixture.AutoMoq;
using AutoFixture;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {

        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _controller;

        public SetPartnerPromoCodeLimitAsyncTests() {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _controller = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerNotFound_Returns404()
        {
            var request = new SetPartnerPromoCodeLimitRequest() { EndDate = DateTime.Now.AddDays(30), Limit = 5 };
            Guid partnerId = Guid.NewGuid();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync((Partner)null);

            IActionResult result = await _controller.SetPartnerPromoCodeLimitAsync(partnerId, request);

            GetStatusCodeFromControllerResult(result).Should().Be(404);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitsAsync_LockedPartner_Returns400()
        {
            var request = new SetPartnerPromoCodeLimitRequest() { EndDate = DateTime.Now.AddDays(30), Limit = 5 };
            Partner inactivePartner = GenerateInactivePartner();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(inactivePartner.Id)).ReturnsAsync(inactivePartner);
            IActionResult result = await _controller.SetPartnerPromoCodeLimitAsync(inactivePartner.Id, request);

            GetStatusCodeFromControllerResult(result).Should().Be(400);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitsAsync_LimitIsActive_SetsPromocodeNumberTo0AndDisablesPrevLimit()
        {
            Partner partner = GeneratePartnerWithLimit(activeLimit: true);
            var prevLimit = partner.PartnerLimits.First();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);
            var request = new SetPartnerPromoCodeLimitRequest() { EndDate = DateTime.Now.AddDays(30), Limit = 5 };

            IActionResult result = await _controller.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            partner.NumberIssuedPromoCodes.Should().Be(0);
            prevLimit.CancelDate.Should().BeOnOrBefore(DateTime.Now);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitsAsync_LimitIsntActive_DoesntChangePromocodeNumber()
        {
            Partner partner = GeneratePartnerWithLimit(activeLimit: false);
            var prevPromocodeNumber = partner.NumberIssuedPromoCodes;
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);
            var request = new SetPartnerPromoCodeLimitRequest() { EndDate = DateTime.Now.AddDays(30), Limit = 5 };

            IActionResult result = await _controller.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            partner.NumberIssuedPromoCodes.Should().Be(prevPromocodeNumber);

        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public async void SetPartnerPromoCodeLimitsAsync_InvalidNewLimit_Returns400(int newLimit)
        {
            Partner partner = GeneratePartnerWithLimit(activeLimit: false);
            var request = new SetPartnerPromoCodeLimitRequest() { EndDate = DateTime.Now.AddDays(30), Limit = newLimit };
            
            IActionResult result = await _controller.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            GetStatusCodeFromControllerResult(result).Should().Be(400);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitsAsync_ValidNewLimit_SavesNewLimit()
        {
            Partner partner = GeneratePartnerWithLimit(activeLimit: false);
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);
            var request = new SetPartnerPromoCodeLimitRequest() { EndDate = DateTime.Now.AddDays(30), Limit = 5 };

            IActionResult result = await _controller.SetPartnerPromoCodeLimitAsync(partner.Id, request);
            _partnersRepositoryMock.Verify(repo => repo.UpdateAsync(It.Is<Partner>(p => p == partner)));

        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public async void SetPartnerPromoCodeLimitsAsync_InvalidNewLimit_DoesntChangeActiveLimit(int newLimit)
        {
            Partner partner = GeneratePartnerWithLimit(activeLimit: true);
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

            var promocodeNumber = partner.NumberIssuedPromoCodes;
            var request = new SetPartnerPromoCodeLimitRequest() { EndDate = DateTime.Now.AddDays(30), Limit = newLimit };

            IActionResult result = await _controller.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            GetStatusCodeFromControllerResult(result).Should().Be(400);
            partner.NumberIssuedPromoCodes.Should().Be(promocodeNumber);
            partner.PartnerLimits.First().CancelDate.Should().BeNull();
        }


        class PartnerPromoCodeLimitBuilder
        {
            private PartnerPromoCodeLimit _result;
            private PartnerPromoCodeLimit Result { 
                get
                {
                    if(_result == null)
                    {
                        _result = new PartnerPromoCodeLimit();
                        _result.Id = Guid.NewGuid();
                    }
                    return _result;
                } 
            }


            public PartnerPromoCodeLimitBuilder WithCreateDate(DateTime createDate)
            {
                Result.CreateDate = createDate;
                return this;
            }

            public PartnerPromoCodeLimitBuilder WithEndDate(DateTime endDate)
            {
                Result.EndDate = endDate;
                return this;
            }

            public PartnerPromoCodeLimit Build()
            {
                var result = Result;
                _result = null;
                return result;
            }
            
        }

        class PartnerBuilder
        {
            private Partner _result;
            private Partner Result
            {
                get
                {
                    if (_result == null)
                    {
                        _result = new Partner();
                        _result.Id = Guid.NewGuid();
                        _result.IsActive = true;
                        _result.PartnerLimits = new List<PartnerPromoCodeLimit>();
                    }
                    return _result;
                }
            }

            public PartnerBuilder WithName(string name)
            {
                Result.Name = name;
                return this;
            }

            public PartnerBuilder Inactive()
            {
                Result.IsActive = false;
                return this;
            }

            public PartnerBuilder WithIssuedPromocodesNumber(int number)
            {
                Result.NumberIssuedPromoCodes = number;
                return this;
            }

            public PartnerBuilder WithLimit(PartnerPromoCodeLimit limit)
            {
                Result.PartnerLimits.Add(limit);
                limit.Partner = Result;
                limit.PartnerId = Result.Id;
                return this;
            }

            public Partner Build()
            {
                var result = Result;
                _result = null;
                return result;
            }
        }

        //Фабричный метод
        private Partner GenerateInactivePartner()
        {
            return new PartnerBuilder()
                       .Inactive()
                       .WithName("Сидоров Иван")
                       .WithIssuedPromocodesNumber(17)
                       .Build();
        }

        //Фабричный метод
        private Partner GeneratePartnerWithLimit(bool activeLimit)
        {
            return new PartnerBuilder()
                       .WithName("Сидоров Иван")
                       .WithIssuedPromocodesNumber(17)
                       .WithLimit(
                            new PartnerPromoCodeLimitBuilder()
                            .WithCreateDate(DateTime.Now - TimeSpan.FromDays(30))
                            .WithEndDate(activeLimit ? DateTime.Now + TimeSpan.FromDays(30) : DateTime.Now - TimeSpan.FromDays(15))
                            .Build()
                       )
                       .Build();
        }

        private int? GetStatusCodeFromControllerResult(IActionResult controllerResult)
        {
            if (controllerResult is StatusCodeResult) return ((StatusCodeResult)controllerResult).StatusCode;
            if (controllerResult is ObjectResult) return ((ObjectResult)controllerResult).StatusCode;
            return null;
        }
    }
} 